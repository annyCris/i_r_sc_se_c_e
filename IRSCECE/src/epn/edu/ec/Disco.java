package epn.edu.ec;

public class Disco extends Cantante {
	//Atributos
			private String genero;
			private int numMusica;
			private double valorDisco;
			private String idiomaDisco;
			private String nombreDisco;
			
			//Constructores
			public Disco(int numMusica, double valorDisco, String idiomaDisco, String nombreDisco) {
				super();
				this.numMusica = numMusica;
				this.valorDisco = valorDisco;
				this.idiomaDisco = idiomaDisco;
				this.nombreDisco= nombreDisco;
			}

			public Disco() {
				super();
				// TODO Auto-generated constructor stub
			}
			
			//Metodos
			public int getNumMusica() {
				return numMusica;
			}

			public void setNumMusica(int numMusica) {
				this.numMusica = numMusica;
			}

			public double getValorDisco() {
				return valorDisco;
			}

			public void setValorDisco(double valorDisco) {
				this.valorDisco = valorDisco;
			}

			public String getIdiomaDisco() {
				return idiomaDisco;
			}

			public void setIdiomaDisco(String idiomaDisco) {
				this.idiomaDisco = idiomaDisco;
			}

			public String getNombreDisco() {
				return nombreDisco;
			}

			public void setNombreDisco(String nombreDisco) {
				this.nombreDisco = nombreDisco;
			}

			public String presentacionBasica(String nombreCantante, int edad, String nombreDisco, double valorDelDisco ) {
				nombreDisco=getNombreDisco();
				valorDelDisco=getValorDisco();
				return ("  Nombre: " +nombreCantante+
						"\n  Edad: "+edad+
						"\n  Nombre Disco: "+nombreDisco+
						"\n  Valor Disco: "+valorDelDisco);
			}
			
			public String presentacionBasica(String genero, int numeroMusicas, String idiomaDelDisco) {		
					
				numeroMusicas=getNumMusica();
				idiomaDelDisco=getIdiomaDisco();
				return ("  Genero de Musica: "+genero+
						"\n  Numero de Musicas: "+numeroMusicas+
						"\n  Idioma del Disco: "+idiomaDelDisco);
			}

	

}
