package epn.edu.ec;

public enum GeneroMusica {
	VALLENATOS, BALADAS, POP, ROCK, HIP_HOP, SALSA, BLUES, JAZZ, BACHATA;
	int a;
	
	// para la inicializacion poniendo posiciones a cada genero ejm (pop(0),rock(1), etc.
	private GeneroMusica(int a) {
		this.a = a;
	}
	
	// para la inicializacion 
	private GeneroMusica() {
		
	}

}
