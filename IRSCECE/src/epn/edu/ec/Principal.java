package epn.edu.ec;

public class Principal {

	public static void main(String[] args) {
		Cantante cantante = new Cantante("Pedro Vicente");
		System.out.println(cantante.tipoEspecialidad());
		System.out.println("Mi informacion es la siguiente: ");
		Disco dis = new Disco(15, 12.2, "Espa�ol","Luna");
		System.out.println(dis.presentacionBasica(cantante.getNombre(), cantante.edad(2017, 1990),dis.getNombreDisco(), dis.getValorDisco()));
		
		System.out.println("\nMas informacion: ");
		
		System.out.println(dis.presentacionBasica(GeneroMusica.BALADAS.name(),dis.getNumMusica(),dis.getIdiomaDisco()));
		
	}

}
