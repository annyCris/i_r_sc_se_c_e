package epn.edu.ec;

public class Cantante implements Persona  {
	//Atributos
	public String nombre;
	
	//Constructor
	public Cantante(String nombre) {
		super();
		this.nombre = nombre;
	}
	public Cantante() {
		super();
		
	}
	
	//Metods
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String tipoEspecialidad() {
		// TODO Auto-generated method stub
		return "... SOY CANTENTE ...";
	}
	
	//No existe error si no se llama a este metodo
	public int edad(int anioActual, int anioNacimiento) {
		int edad=anioActual-anioNacimiento;
		// TODO Auto-generated method stub
		return edad;
	}
}
